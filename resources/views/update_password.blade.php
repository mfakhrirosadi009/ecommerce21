<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Update Password</title>
</head>

<body>
  @if($errors->any())
  @foreach($errors->all() as $error)
  <p>{{$error}}</p>
  @endforeach
  @endif

  @if(Session::has('messege'))
  <p>{{Session::get('messege')}}</p>
  @endif
  <form action="{{route('store_password')}}" method="post">
    @method('path')
    @csrf
    <input type="text" name="new_password"><br>
    <input type="password" name="new_password_confirmation"><br>
    <button type="submit">Change</button>
  </form>
</body>

</html>