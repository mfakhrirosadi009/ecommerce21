@extends('template.layout')

@section('content')
<div class="row">
  <div class="col-lg-12 margin-tb">
    <div class="pull-left">
      <h2> Show Transaksi</h2>
    </div>
    <div class="pull-right">
      <a class="btn btn-primary" href="{{route('transaksi.index') }}"> Back</a>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-xs-12 col-sm-12 col-md-12">
    <div class="form-group">
      <strong>Id pelanggan:</strong>
      {{ $transaksi->id_pelanggan }}
    </div>
  </div>
  <div class="col-xs-12 col-sm-12 col-md-12">
    <div class="form-group">
      <strong>Id produk:</strong>
      {{ $transaksi->id_produk }}
    </div>
  </div>
  <div class="col-xs-12 col-sm-12 col-md-12">
    <div class="form-group">
      <strong>Jumlah:</strong>
      {{ $transaksi->jumlah }}
    </div>
  </div>
  <div class="col-xs-12 col-sm-12 col-md-12">
    <div class="form-group">
      <strong>Harga:</strong>
      {{ $transaksi->harga }}
    </div>
  </div>
</div>
@endsection