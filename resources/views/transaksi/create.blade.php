@extends('template.layout')
@section('content')
<div class="row">
  <div class="col-lg-12 margin-tb">
    <div class="pull-left">
      <h2>Add New Transaksi</h2>
    </div>
    <div class="pull-right">
      <a class="btn btn-primary" href="{{ 
route('products.index') }}"> Back</a>
    </div>
  </div>
</div>

@if ($errors->any())
<div class="alert alert-danger">
  <strong>Whoops!</strong> There were some problems with your
  input.<br><br>
  <ul>
    @foreach ($errors->all() as $error)
    <li>{{ $error }}</li>
    @endforeach
  </ul>
</div>
@endif

<form action="{{ route('products.store') }}" method="POST">
  @csrf

  <div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12">
      <div class="form-group">
        <strong>Id pelanggan:</strong>
        <input type="text" name="id_pelanggan" class="form-control" placeholder="id_pelanggan">
      </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
      <div class="form-group">
        <strong>Id produk:</strong>
        <textarea class="form-control" style="height:150px" name="detail" placeholder="Detail"></textarea>
      </div>
      <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
          <strong>Jumlah:</strong>
          <textarea class="form-control" style="height:150px" name="jumlah" placeholder="jumlah"></textarea>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
          <div class="form-group">
            <strong>Harga:</strong>
            <textarea class="form-control" style="height:150px" name="harga" placeholder="harga"></textarea>
          </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
          <button type="submit" class="btn btnprimary">Submit</button>
        </div>
      </div>

</form>
@endsection