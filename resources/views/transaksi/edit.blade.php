@extends('template.layout')

@section('content')
<div class="row">
  <div class="col-lg-12 margin-tb">
    <div class="pull-left">
      <h2>Edit transaksi</h2>
    </div>
    <div class="pull-right">
      <a class="btn btn-primary" href="{{route('transaksi.index') }}"> Back</a>
    </div>
  </div>
</div>
@if ($errors->any())
<div class="alert alert-danger">
  <strong>Whoops!</strong> There were some problems with your
  input.<br><br>
  <ul>
    @foreach ($errors->all() as $error)
    <li>{{ $error }}</li>
    @endforeach
  </ul>
</div>
@endif

<form action="{{ route('transaksi.update',$transaksi->id) }}" method="POST">
  @csrf
  @method('PUT')

  <div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12">
      <div class="form-group">
        <strong>Id transaksi:</strong>
        <input type="text" name="id_transaksi" value="{{ $transaksi->id_transaksi }}" class="form-control" placeholder="id_transaksi">
      </div>
    </div>

    <div class="col-xs-12 col-sm-12 col-md-12">
      <div class="form-group">
        <strong>Id produk:</strong>
        <input type="text" name="id_produk" value="{{ $transaksi->id_produk }}" class="form-control" placeholder="id_produk">
      </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
      <div class="form-group">
        <strong>Jumlah:</strong>
        <input type="text" name="jumlah" value="{{ $transaksi->jumlah }}" class="form-control" placeholder="jumlah">
      </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
      <div class="form-group">
        <strong>Harga:</strong>
        <input type="text" name="harga" value="{{ $transaksi->harga }}" class="form-control" placeholder="harga">
      </div>
    </div>

    <div class="col-xs-12 col-sm-12 col-md-12 textcenter">
      <button type="submit" class="btn btnprimary">Submit</button>
    </div>
  </div>
</form>
@endsection