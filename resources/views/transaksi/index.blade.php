@extends('template.layout')
@section('content')
<div class="row">
  <div class="col-lg-12 margin-tb">
    <div class="pull-left">
      <h2>Laravel 9 CRUD Example from scratch - ItSolutionStuff.com</h2>
    </div>
    <div class="pull-right">
      <a class="btn btn-success" href="{{ route('transaksi.create') }}"> Create New transaksi</a>
    </div>
  </div>
</div>

@if ($message = Session::get('success'))
<div class="alert alert-success">
  <p>{{ $message }}</p>
</div>
@endif
<table class="table table-bordered">
  <tr>
    <th>No</th>
    <th>Id pelanggan</th>
    <th>Id produk</th>
    <th>Jumlah</th>
    <th>Harga</th>
    <th width="280px">Action</th>
  </tr>
  @foreach ($pelanggan as $product)
  <tr>
    <td>{{ ++$i }}</td>
    <td>{{ $transaksi->id_pelanggan }}</td>
    <td>{{ $transaksi->id_produk }}</td>
    <td>{{ $transaksi->jumlah }}</td>
    <td>{{ $transaksi->harga }}</td>
    <td>
      <form action="{{ route('transaksi.destroy',$transaksi->id) }}" method="POST">

        <a class="btn btn-info" href="{{ 
route('transaksi.show',$transaksi->id) }}">Show</a>

        <a class="btn btn-primary" href="{{route('transaksi.edit',$transaksi->id) }}">Edit</a>

        @csrf
        @method('DELETE')

        <button type="submit" class="btn btn-danger">Delete</button>
      </form>
    </td>
  </tr>
  @endforeach
</table>

{!! $transaksi->links() !!}

@endsection